(function() {
	var app = angular.module('store-products', []);
	var BASE_URL = 'http://10.0.33.34:3000/api/products';
	var header = { headers: {'Content-Type': 'application/json'}};

	app.controller('StoreController', ['$http', '$filter', function($http, $filter){
		var store = this;
		store.products = [];
		$http.get(BASE_URL).success(function(data){
			store.products = data;
		});

		store.newproduct = {};
		store.addProduct = function() {
			$http.post(BASE_URL, store.newproduct, header)
			.success(function(data, status){
				store.newproduct = {};
				listProducts();
			})
			.error(function(data, status){
				console.log("Fail:" + data + status);
			});
		};

		store.deleteProduct = function(product) {
			var DELETE_URL = BASE_URL + '/' + product._id;
			$http.delete(DELETE_URL, header)
			.success(function(data, status){
				listProducts();
			})
			.error(function(data, status){
				console.log("Fail:" + data + status);
			});
		}

		store.updateProduct = function(product) {
			var UPDATE_URL = BASE_URL + '/' + product._id;
			$http.put(UPDATE_URL, product, header)
			.success(function(data, status){
				listProducts();
			})
			.error(function(data, status){
				console.log("Fail:" + data + status);
			});
		}

		function listProducts() {
			$http.get(BASE_URL).success(function(data){
				store.products = data;
			});
		};
	}]);
})();