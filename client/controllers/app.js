(function() {
	var app = angular.module('store', ['store-products']);

	app.controller("PanelController", function(){
		
		this.selectTab = function(setTab) {
			this.tab = setTab;
		};

		this.isSelected = function(checkTab) {
			return this.tab === checkTab;
		};
	});
})();